package au.com.anz.test.factory;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.PlaceCommand;
import au.com.anz.test.script.ScriptElements;

public class PlaceCommandBuilderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testNewCommand() {
		CommandBuilder builder = new PlaceCommandBuilder();

		Command command = builder.newCommand();

		assertTrue(command.getClass().equals(PlaceCommand.class));
	}

	@Test
	public final void testCreate() {
		CommandBuilder builder = new PlaceCommandBuilder();

		Command command = builder.create(ScriptElements.PLACE);

		assertTrue(command.getClass().equals(PlaceCommand.class));
	}

}
