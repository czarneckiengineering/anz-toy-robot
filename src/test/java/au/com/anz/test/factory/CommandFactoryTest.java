package au.com.anz.test.factory;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.MoveCommand;
import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.script.ScriptElements;

public class CommandFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testCreate() throws InvalidScriptElement {
		CommandFactory factory = new CommandFactory();

		Command command = factory.create(ScriptElements.MOVE);

		assertTrue(command.getClass().equals(MoveCommand.class));
	}

}
