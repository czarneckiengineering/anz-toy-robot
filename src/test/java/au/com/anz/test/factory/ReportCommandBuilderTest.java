package au.com.anz.test.factory;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.ReportCommand;
import au.com.anz.test.script.ScriptElements;

public class ReportCommandBuilderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testNewCommand() {
		CommandBuilder builder = new ReportCommandBuilder();

		Command command = builder.newCommand();

		assertTrue(command.getClass().equals(ReportCommand.class));
	}

	@Test
	public final void testCreate() {
		CommandBuilder builder = new ReportCommandBuilder();

		Command command = builder.create(ScriptElements.REPORT);

		assertTrue(command.getClass().equals(ReportCommand.class));
	}

}
