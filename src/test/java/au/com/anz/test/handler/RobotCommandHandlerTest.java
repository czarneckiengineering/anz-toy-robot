package au.com.anz.test.handler;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.command.PlaceCommand;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.Table;

public class RobotCommandHandlerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testRobotCommandHandler() {
		Table table = new Table();

		RobotCommandHandler handler = new RobotCommandHandler(table);

		assertEquals(handler.getTable(), table);
	}

	@Test
	public final void testHandle() throws InvalidStateException {
		Table table = new Table();

		RobotCommandHandler handler = new RobotCommandHandler(table);

		handler.handle(new PlaceCommand());

		assertEquals(handler.getTable(), table);
	}


}
