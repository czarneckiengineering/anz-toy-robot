package au.com.anz.test.command;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;
import au.com.anz.test.state.Table;

public class ReportCommandTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testExecute() throws InvalidStateException {
		Command command = new ReportCommand();

		Table table = new Table();
		table.setState (new State(0,0,Direction.NORTH));
		CommandHandler handler = new RobotCommandHandler(table);
		command.execute(handler);

		assertEquals(table.getState().getDirection(), Direction.NORTH);
	}

}
