package au.com.anz.test.command;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.script.Script;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.Table;

public class PlaceCommandTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testExecute() throws InvalidStateException {
		Command command = new PlaceCommand();

		Table table = new Table();
		CommandHandler handler = new RobotCommandHandler(table);
		command.execute(handler);

		assertEquals(table.getState().getX(), 0);
		assertEquals(table.getState().getY(), 0);
		assertEquals(table.getState().getDirection(), Direction.NORTH);
	}

	@Test
	public final void testPostConstruct() throws InvalidScriptElement, InvalidStateException {
		Command command = new PlaceCommand();

		command.postConstruct(new Script(new String[] {"1", "1", "EAST"}));

		Table table = new Table();
		CommandHandler handler = new RobotCommandHandler(table);
		command.execute(handler);

		assertEquals(table.getState().getX(), 1);
		assertEquals(table.getState().getY(), 1);
		assertEquals(table.getState().getDirection(), Direction.EAST);
	}

}
