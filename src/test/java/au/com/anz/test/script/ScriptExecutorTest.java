package au.com.anz.test.script;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.factory.CommandFactory;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.Table;

public class ScriptExecutorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testExecute() throws InvalidStateException {
		CommandFactory factory = new CommandFactory();
		ScriptExecutor executor = new ScriptExecutor(factory);

		Table table = new Table();
		Script script = new Script(new String[] {"PLACE", "0", "0", "NORTH"});
		executor.execute(script, table);

		assertEquals(table.getState().getX(), 0);
		assertEquals(table.getState().getY(), 0);
		assertEquals(table.getState().getDirection(), Direction.NORTH);
	}

}
