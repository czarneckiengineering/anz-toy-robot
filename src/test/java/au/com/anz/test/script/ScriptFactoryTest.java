package au.com.anz.test.script;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ScriptFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testCreate() throws IOException {
		ScriptFactory factory = new ScriptFactory();
		Script script = factory.create(new FileReader ("src\\test\\resources\\test1"));

		assertTrue(script.nextToken() != null);

	}

}
