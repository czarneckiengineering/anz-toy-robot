package au.com.anz.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.factory.CommandFactory;
import au.com.anz.test.script.Script;
import au.com.anz.test.script.ScriptExecutor;
import au.com.anz.test.script.ScriptFactory;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;
import au.com.anz.test.state.Table;

public class ToyRobotFactoriesTest {

	private static ScriptFactory scriptFactory;
	private static CommandFactory commandFactory;
	private static ScriptExecutor executor;

	private Script script;
	private Table table;
	
	private static String test1 = "PLACE 0,0,NORTH MOVE REPORT";
	private static String test2 = "PLACE 0,0,NORTH LEFT REPORT";
	private static String test3 = "PLACE 1,2,EAST MOVE MOVE LEFT MOVE REPORT";
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		scriptFactory = new ScriptFactory();
		commandFactory = new CommandFactory();
		executor = new ScriptExecutor(commandFactory);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		table = new Table();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testScriptOne() throws IOException, InvalidStateException {
		script = scriptFactory.create(new StringReader(test1));
		executor.execute(script, table);
		
		assertEquals(table.getState(), new State(0,1,Direction.NORTH));
	}

	@Test
	public final void testScriptTwo() throws IOException, InvalidStateException {
		script = scriptFactory.create(new StringReader(test2));
		executor.execute(script, table);
		
		assertEquals(table.getState(), new State(0,0,Direction.WEST));
	}

	@Test
	public final void testScriptThree() throws IOException, InvalidStateException {
		script = scriptFactory.create(new StringReader(test3));
		executor.execute(script, table);
		
		assertEquals(table.getState(), new State(3,3,Direction.NORTH));
	}

}
