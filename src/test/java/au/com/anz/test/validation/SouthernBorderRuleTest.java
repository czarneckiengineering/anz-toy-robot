package au.com.anz.test.validation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;

public class SouthernBorderRuleTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testValidate() throws InvalidStateException {
		State state = new State(3,3,Direction.NORTH);

		Validator validator = new SouthernBorderRule();
		validator.validate(state);
	}

}
