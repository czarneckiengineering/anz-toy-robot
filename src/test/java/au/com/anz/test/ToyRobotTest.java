package au.com.anz.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ToyRobotTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testScriptOne() {
		ToyRobot.main(new String[]{"src\\test\\resources\\test1"});
	}

	@Test
	public final void testScriptTwo() {
		ToyRobot.main(new String[]{"src\\test\\resources\\test2"});
	}

	@Test
	public final void testScriptThree() {
		ToyRobot.main(new String[]{"src\\test\\resources\\test3"});
	}

	@Test
	public final void testInvalidScriptFile() {
		ToyRobot.main(new String[]{"src\\test\\resources\\test4"});
	}

}
