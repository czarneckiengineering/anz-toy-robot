package au.com.anz.test.state;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testState() {
		State state = new State (1,1,Direction.EAST);

		assertEquals(state.getX(),1);
		assertEquals(state.getY(),1);
		assertEquals(state.getDirection(),Direction.EAST);
	}

}
