package au.com.anz.test.state;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidStateException;

public class TableTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testSetGetState() throws InvalidStateException {
		Table table = new Table();

		State state = new State(0,0,Direction.NORTH);

		table.setState(state);

		assertEquals(table.getState(), state);
	}

}
