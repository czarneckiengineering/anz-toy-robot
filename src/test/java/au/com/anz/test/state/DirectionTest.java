package au.com.anz.test.state;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.script.ScriptElements;

public class DirectionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testClockwise() throws InvalidStateException {
		assertEquals(Direction.clockwise(Direction.NORTH), Direction.EAST);
		assertEquals(Direction.clockwise(Direction.EAST), Direction.SOUTH);
		assertEquals(Direction.clockwise(Direction.SOUTH), Direction.WEST);
		assertEquals(Direction.clockwise(Direction.WEST), Direction.NORTH);
	}

	@Test
	public final void testAnticlockwise() throws InvalidStateException {
		assertEquals(Direction.anticlockwise(Direction.EAST), Direction.NORTH);
		assertEquals(Direction.anticlockwise(Direction.NORTH), Direction.WEST);
		assertEquals(Direction.anticlockwise(Direction.WEST), Direction.SOUTH);
		assertEquals(Direction.anticlockwise(Direction.SOUTH), Direction.EAST);
	}

	@Test
	public final void testToStringInt() throws InvalidStateException {
		assertEquals(Direction.toString(Direction.NORTH),ScriptElements.NORTH);
		assertEquals(Direction.toString(Direction.EAST),ScriptElements.EAST);
		assertEquals(Direction.toString(Direction.SOUTH),ScriptElements.SOUTH);
		assertEquals(Direction.toString(Direction.WEST),ScriptElements.WEST);
	}

	@Test
	public final void testParseDirection() throws InvalidScriptElement {
		assertEquals(Direction.parseDirection(ScriptElements.NORTH), Direction.NORTH);
		assertEquals(Direction.parseDirection(ScriptElements.EAST), Direction.EAST);
		assertEquals(Direction.parseDirection(ScriptElements.SOUTH), Direction.SOUTH);
		assertEquals(Direction.parseDirection(ScriptElements.WEST), Direction.WEST);
	}

}
