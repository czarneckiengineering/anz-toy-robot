package au.com.anz.test;

import java.io.FileReader;
import java.io.IOException;

import au.com.anz.test.factory.CommandFactory;
import au.com.anz.test.script.Script;
import au.com.anz.test.script.ScriptExecutor;
import au.com.anz.test.script.ScriptFactory;
import au.com.anz.test.state.Table;

/**
 * The Class ToyRobot.
 */
public class ToyRobot {

	/**
	 * The main method.
	 *
	 * @param args takes a script filename as a parameter
	 */
	public static void main(String[] args) {

		try {
			ScriptFactory scriptFactory = new ScriptFactory();
			Script script = scriptFactory.create(new FileReader(args[0]));

			Table table = new Table();

			CommandFactory commandFactory = new CommandFactory();
			ScriptExecutor executor = new ScriptExecutor(commandFactory);
			executor.execute(script, table);
		}
		catch (IOException e) {
			
		}
	}

}
