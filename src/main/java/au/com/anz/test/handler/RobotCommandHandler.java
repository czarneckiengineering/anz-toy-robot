package au.com.anz.test.handler;

import au.com.anz.test.command.Command;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.Table;

public class RobotCommandHandler implements CommandHandler {

	private Table table;

	public RobotCommandHandler(Table table) {
		this.table = table;
	}

	public void handle(Command command) throws InvalidStateException {
		command.execute(this);
	}

	public Table getTable() {
		return table;
	}

}
