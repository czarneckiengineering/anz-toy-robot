package au.com.anz.test.handler;

import au.com.anz.test.command.Command;
import au.com.anz.test.exception.InvalidStateException;

public interface CommandHandler {

	void handle (Command command) throws InvalidStateException;

}
