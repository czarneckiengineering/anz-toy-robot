package au.com.anz.test.command;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.script.Script;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;
import au.com.anz.test.state.Table;

public class ReportCommand implements Command {

	public void execute(CommandHandler handler) throws InvalidStateException {
		try {
			RobotCommandHandler robotHandler = (RobotCommandHandler)handler;
			Table table = robotHandler.getTable();

			State state = table.getState();

			System.out.println(state.getX() + "," + state.getY() + "," + Direction.toString (state.getDirection()));
		}
		catch (ClassCastException e) {

		}
	}

	public void postConstruct(Script script) throws InvalidScriptElement {
	}

}
