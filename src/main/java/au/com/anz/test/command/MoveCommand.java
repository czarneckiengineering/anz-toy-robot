package au.com.anz.test.command;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.script.Script;
import au.com.anz.test.state.Table;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;

public class MoveCommand implements Command {

	public void execute(CommandHandler handler) throws InvalidStateException {
		try {
			RobotCommandHandler robotHandler = (RobotCommandHandler)handler;
			Table table = robotHandler.getTable();
			State state = table.getState();

			int x = state.getX();
			int y = state.getY();

			switch (state.getDirection()) {
			case Direction.NORTH:
				y++;
				break;
			case Direction.EAST:
				x++;
				break;
			case Direction.SOUTH:
				y--;
				break;
			case Direction.WEST:
				x--;
				break;
			}

			State newState = new State (x, y, state.getDirection());

			table.setState(newState);
		}
		catch (ClassCastException e) {

		}
	}

	public void postConstruct(Script script) throws InvalidScriptElement {
	}

}
