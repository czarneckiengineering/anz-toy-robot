package au.com.anz.test.command;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.script.Script;

public interface Command {

	void execute(CommandHandler handler) throws InvalidStateException;

	void postConstruct(Script script) throws InvalidScriptElement;

}
