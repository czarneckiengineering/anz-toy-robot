package au.com.anz.test.command;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.handler.CommandHandler;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.script.Script;
import au.com.anz.test.state.Direction;
import au.com.anz.test.state.State;
import au.com.anz.test.state.Table;

public class PlaceCommand implements Command {

	private int x;
	private int y;
	private int direction;

	public void execute(CommandHandler handler) throws InvalidStateException {
		try {
			RobotCommandHandler robotHandler = (RobotCommandHandler)handler;
			Table table = robotHandler.getTable();

			State newState = new State (x, y, direction);

			table.setState(newState);
		}
		catch (ClassCastException e) {

		}
	}

	public void postConstruct(Script script) throws InvalidScriptElement {
		try {
			x = Integer.parseInt(script.nextToken());
			y = Integer.parseInt(script.nextToken());
		}
		catch (NumberFormatException e) {

		}
		direction = Direction.parseDirection(script.nextToken());
	}

}
