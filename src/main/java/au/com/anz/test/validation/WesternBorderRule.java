package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public class WesternBorderRule implements Validator {

	private int west = 0;

	public void validate(State state) throws InvalidStateException {
		if (state.getX() < west)
			throw new InvalidStateException("Invalid position");
	}

}
