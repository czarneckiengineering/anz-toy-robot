package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public class EasternBorderRule implements Validator {

	private int east = 5;

	public void validate(State state) throws InvalidStateException {
		if (state.getX() > east)
			throw new InvalidStateException("Invalid position");
	}

}
