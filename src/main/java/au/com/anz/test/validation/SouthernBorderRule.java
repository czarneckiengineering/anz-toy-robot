package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public class SouthernBorderRule implements Validator {

	private int south = 0;

	public void validate(State state) throws InvalidStateException {
		if (state.getY() < south)
			throw new InvalidStateException("Invalid position");
	}

}
