package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public interface Validator {

	void validate (State state) throws InvalidStateException;

}
