package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public class NorthernBorderRule implements Validator {

	private int north = 5;

	public void validate(State state) throws InvalidStateException {
		if (state.getY() > north)
			throw new InvalidStateException("Invalid position");
	}

}
