package au.com.anz.test.validation;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.state.State;

public class TableValidator implements Validator {

	private static Validator[] validators = {
		new SouthernBorderRule(),
		new NorthernBorderRule(),
		new WesternBorderRule(),
		new EasternBorderRule()
	};

	public void validate(State state) throws InvalidStateException {
		for (Validator validator : validators) {
			validator.validate(state);
		}
	}

}
