package au.com.anz.test.exception;

public class InvalidStateException extends RobotException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7199837855133882886L;

	public InvalidStateException() {
	}

	public InvalidStateException(String message) {
		super(message);
	}

	public InvalidStateException(Throwable cause) {
		super(cause);
	}

	public InvalidStateException(String message, Throwable cause) {
		super(message, cause);
	}

}
