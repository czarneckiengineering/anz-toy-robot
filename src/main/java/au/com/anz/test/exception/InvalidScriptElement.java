package au.com.anz.test.exception;

public class InvalidScriptElement extends RobotException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7199837855133882886L;

	public InvalidScriptElement() {
	}

	public InvalidScriptElement(String message) {
		super(message);
	}

	public InvalidScriptElement(Throwable cause) {
		super(cause);
	}

	public InvalidScriptElement(String message, Throwable cause) {
		super(message, cause);
	}

}
