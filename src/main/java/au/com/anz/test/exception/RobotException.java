package au.com.anz.test.exception;

public class RobotException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 4058888746148918020L;

	public RobotException() {
	}

	public RobotException(String message) {
		super(message);
	}

	public RobotException(Throwable cause) {
		super(cause);
	}

	public RobotException(String message, Throwable cause) {
		super(message, cause);
	}

}
