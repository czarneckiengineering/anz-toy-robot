package au.com.anz.test.script;

public class ScriptElements {

	public final static String PLACE = "PLACE";
	public final static String MOVE = "MOVE";
	public final static String LEFT = "LEFT";
	public final static String RIGHT = "RIGHT";
	public final static String REPORT = "REPORT";
	public final static String NORTH = "NORTH";
	public final static String EAST = "EAST";
	public final static String SOUTH = "SOUTH";
	public final static String WEST = "WEST";

}
