package au.com.anz.test.script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class ScriptFactory {

	public Script create(Reader reader) throws IOException {
		String text = "";

		BufferedReader in = new BufferedReader(reader);
		String line;
		while ((line = in.readLine()) != null) {
			text = text + " " + line;
		}
		in.close();

		String[] tokens = text.split("\\W");

		return new Script(tokens);
	}

}
