package au.com.anz.test.script;

import au.com.anz.test.command.Command;
import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.factory.CommandFactory;
import au.com.anz.test.handler.RobotCommandHandler;
import au.com.anz.test.state.Table;

public class ScriptExecutor {

	private CommandFactory factory;

	public ScriptExecutor(CommandFactory factory) {
		super();
		this.factory = factory;
	}

	public void execute (Script script, Table table) {
		RobotCommandHandler handler = new RobotCommandHandler(table);

		for (String token = script.nextToken(); token != null; token = script.nextToken()) {
			try {
				Command command = factory.create(token);
				command.postConstruct(script);

				handler.handle(command);
			}
			catch (InvalidScriptElement e) {
			}
			catch (InvalidStateException e) {
			}
		}
	}

}
