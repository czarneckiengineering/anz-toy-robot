package au.com.anz.test.script;

public class Script {

	private String[] tokens = null;

	private int position = 0;

	public Script(String[] tokens) {
		super();
		this.tokens = tokens;
	}

	public String nextToken() {
		if (tokens != null && position < tokens.length) {
			return (tokens[position++]);
		}

		return null;
	}

}
