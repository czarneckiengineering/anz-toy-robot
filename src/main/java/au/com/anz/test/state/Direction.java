package au.com.anz.test.state;

import au.com.anz.test.exception.InvalidScriptElement;
import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.script.ScriptElements;

public class Direction {

	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;

	public static int clockwise (int direction) throws InvalidStateException {
		switch (direction) {
		case NORTH:
			return EAST;
		case EAST:
			return SOUTH;
		case SOUTH:
			return WEST;
		case WEST:
			return NORTH;
		}

		throw new InvalidStateException ("Invalid value for direction");
	}

	public static int anticlockwise (int direction) throws InvalidStateException {
		switch (direction) {
		case NORTH:
			return WEST;
		case WEST:
			return SOUTH;
		case SOUTH:
			return EAST;
		case EAST:
			return NORTH;
		}

		throw new InvalidStateException ("Invalid value for direction");
	}

	public static String toString(int direction) throws InvalidStateException {
		switch (direction) {
		case 0:
			return ScriptElements.NORTH;
		case 1:
			return ScriptElements.EAST;
		case 2:
			return ScriptElements.SOUTH;
		case 3:
			return ScriptElements.WEST;
		}

		throw new InvalidStateException ("" + direction + " is not a valid direction");
	}

	public static int parseDirection(String direction) throws InvalidScriptElement {
		if (ScriptElements.NORTH.equals(direction))
			return NORTH;
		if (ScriptElements.EAST.equals(direction))
			return EAST;
		if (ScriptElements.SOUTH.equals(direction))
			return SOUTH;
		if (ScriptElements.WEST.equals(direction))
			return WEST;

		throw new InvalidScriptElement (direction + " is not a valid direction");
	}

}
