package au.com.anz.test.state;

import au.com.anz.test.exception.InvalidStateException;
import au.com.anz.test.validation.TableValidator;
import au.com.anz.test.validation.Validator;

public class Table {

	private Validator validator = new TableValidator();

	private State state;

	public Table() {
	}

	public State getState() throws InvalidStateException {
		if (state == null)
			throw new InvalidStateException("The robot has not been placed on the table");

		return state;
	}

	public void setState(State state) throws InvalidStateException {
		validate (state);

		this.state = state;
	}

	private void validate (State state) throws InvalidStateException {
		validator.validate(state);
	}

}
