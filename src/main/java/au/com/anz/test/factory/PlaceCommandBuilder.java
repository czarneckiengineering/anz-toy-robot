package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.PlaceCommand;
import au.com.anz.test.script.ScriptElements;

public class PlaceCommandBuilder extends CommandBuilder {

	public PlaceCommandBuilder() {
		super (ScriptElements.PLACE);
	}

	@Override
	protected Command newCommand() {
		return new PlaceCommand();
	}

}
