package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.exception.InvalidScriptElement;

public class CommandFactory {

	static private CommandBuilder[] builders = {
			new PlaceCommandBuilder(),
			new LeftCommandBuilder(),
			new RightCommandBuilder(),
			new MoveCommandBuilder(),
			new ReportCommandBuilder()
		};

	public CommandFactory() {
	}

	public Command create(String token) throws InvalidScriptElement {
		for (CommandBuilder builder : builders) {
			Command command = builder.create(token);

			if (command != null)
				return command;
		}

		throw new InvalidScriptElement ("Token '" + token + "' was not recognised");
	}

}
