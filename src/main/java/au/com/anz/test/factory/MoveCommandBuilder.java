package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.MoveCommand;
import au.com.anz.test.script.ScriptElements;

public class MoveCommandBuilder extends CommandBuilder {

	public MoveCommandBuilder() {
		super (ScriptElements.MOVE);
	}

	@Override
	protected Command newCommand() {
		return new MoveCommand();
	}

}
