package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.ReportCommand;
import au.com.anz.test.script.ScriptElements;

public class ReportCommandBuilder extends CommandBuilder {

	public ReportCommandBuilder() {
		super (ScriptElements.REPORT);
	}

	@Override
	protected Command newCommand() {
		return new ReportCommand();
	}

}
