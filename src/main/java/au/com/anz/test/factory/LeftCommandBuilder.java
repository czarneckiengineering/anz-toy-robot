package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.LeftCommand;
import au.com.anz.test.script.ScriptElements;

public class LeftCommandBuilder extends CommandBuilder {

	public LeftCommandBuilder() {
		super (ScriptElements.LEFT);
	}

	@Override
	protected Command newCommand() {
		return new LeftCommand();
	}

}
