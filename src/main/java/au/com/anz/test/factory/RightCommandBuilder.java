package au.com.anz.test.factory;

import au.com.anz.test.command.Command;
import au.com.anz.test.command.RightCommand;
import au.com.anz.test.script.ScriptElements;

public class RightCommandBuilder extends CommandBuilder {

	public RightCommandBuilder() {
		super (ScriptElements.RIGHT);
	}

	@Override
	protected Command newCommand() {
		return new RightCommand();
	}

}
