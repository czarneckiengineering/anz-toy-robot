package au.com.anz.test.factory;

import au.com.anz.test.command.Command;

public abstract class CommandBuilder {

	String key;

	public CommandBuilder(String key) {
		super();
		this.key = key;
	}

	public Command create (String key) {
		if (key.equals(this.key))
			return newCommand();

		return null;
	}

	protected abstract Command newCommand ();

}
