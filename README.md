# Toy Robot Test #

The code to solve the Toy Robot Test exercise and the three core tests is built and executed in a Maven command

~~~
mvn clean install cobertura:cobertura
~~~

## Design Notes ##

The entry point to the solution is in the class ToyRobot. The main method here takes a single parameter, the path to a script file that conforms to the rules defined.

The code then divides into the interpretation of the script file, as a list of commands built by a Factory and a set of Builders; and the execution of the generated commands as actions received by a CommandHandler on the state stored in an instance of the Table class.

Validation rules have been extracted to a class hierarchy under the validation package.

## Execution Notes ##

The three defined tests of the system can be executed from the command line or by executing the JUnit test ToyRobotTest from within an IDE or from a Maven execution.

The three defined tests are also coded inside the ToyRobotFactoriesTest which shows direct use of the factories, and allows deeper validation of test results than the StdOut from the basic test.

The wider test suite of the solution code is most easily tested via maven by the command line above

The source code can then be graphically explored through the Cobertura output.
